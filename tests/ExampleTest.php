<?php


namespace Tests;


use Kata\Example;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    public function test_it_works()
    {
        $this->assertTrue((new Example())->checkMe());
    }
}
